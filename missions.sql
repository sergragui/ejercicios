-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Temps de generació: 20-02-2021 a les 12:48:12
-- Versió del servidor: 10.4.17-MariaDB
-- Versió de PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `missions`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `quests`
--

CREATE TABLE `quests` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `num_vots` int(11) NOT NULL,
  `mitjana` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `quests`
--

INSERT INTO `quests` (`id`, `descripcion`, `num_vots`, `mitjana`) VALUES
(1, 'Movimiento naranja', 5, '2.30'),
(2, 'No se sale, pero a votar si', 7, '3.20'),
(3, 'Transporte publico', 9, '2.50'),
(4, 'Raid: El casoplón del coletas', 8, '3.70'),
(5, 'Mission Imposible: Prácticas', 4, '5.00');

-- --------------------------------------------------------

--
-- Estructura de la taula `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Nick` varchar(15) NOT NULL,
  `passwd` varchar(20) NOT NULL,
  `Score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `users`
--

INSERT INTO `users` (`id`, `Nombre`, `Apellido`, `Nick`, `passwd`, `Score`) VALUES
(1, 'Sergi', 'Grau', 'sergragui', 'sergigrau', 1),
(2, 'Cristian', 'Ramirez', 'caramelorex', 'caramelo', 0),
(3, 'Alberto', 'Cabrera', 'Tormento23', 'tormento', 6),
(4, 'Pau', 'Salto', 'psalto', 'salto', 3),
(5, 'Javier', 'Lizandra', 'javichu', 'javi123', 4),
(9, 'Xavi', 'Garcia', 'xavgar', '12341', 7),
(10, 'Alex', 'Ruiz', 'eldergork', '123451', 5);

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `quests`
--
ALTER TABLE `quests`
  ADD PRIMARY KEY (`id`);

--
-- Índexs per a la taula `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `quests`
--
ALTER TABLE `quests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la taula `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
