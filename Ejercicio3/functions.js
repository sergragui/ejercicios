
function showMessage(){
    window.location="./page2.php";
}

function showUser(){
    window.location="./pageUsers.php";
}

function showUserScore(){
    window.location="./pageUsersScore.php";
}

function loadEvents(){
    document.getElementById("searchQuest").addEventListener("click", showMessage);
    document.getElementById("searchUser").addEventListener("click", showUser);
    document.getElementById("searchScore0").addEventListener("click", showUserScore);
}