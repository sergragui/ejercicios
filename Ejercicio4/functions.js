
function showQuests(){
    window.location="./pageQuests.php";
}

function showUser(){
    window.location="./pageUsers.php";
}

function showUserScore(){
    window.location="./pageUsersScore.php";
}

function loadEvents(){
    document.getElementById("searchQuest").addEventListener("click", showQuests);
    document.getElementById("searchUser").addEventListener("click", showUser);
    document.getElementById("searchScore0").addEventListener("click", showUserScore);
}