
function showQuests(){
    window.location="./pageQuests.php";
}

function showUser(){
    window.location="./pageUsers.php";
}

function showUserScore(){
    window.location="./pageUsersScore.php";
}

function showUserId(){
    window.location="./pageUsersId.php?number=" + document.getElementById("questId").value;
}

function loadEvents(){
    document.getElementById("searchQuest").addEventListener("click", showQuests);
    document.getElementById("searchUser").addEventListener("click", showUser);
    document.getElementById("searchScore0").addEventListener("click", showUserScore);
    document.getElementById("searchUserId").addEventListener("click", showUserId);
}